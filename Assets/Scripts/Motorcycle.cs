using System;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "Motorcycle", menuName = "ScriptableObjects/SpawnMotorcycle", order = 1)]
    public class Motorcycle : Vehicle
    {
        [Serializable]
        public class Dimensions
        {
            public int Length;
            public int Height;
            public int Width;
        }
        
        [SerializeField] private Dimensions DimensionsSize;
        
        public override string GetField4()
        {
            return $"Габариты - {DimensionsSize.Length}x{DimensionsSize.Height}x{DimensionsSize.Width}";
        }
    }
}