using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "Auto", menuName = "ScriptableObjects/SpawnAuto", order = 1)]
    public class Auto : Vehicle
    {
        [SerializeField] private string Color;
        
        public override string GetField4()
        {
            return $"Цвет - {Color}";
        }
    }
}