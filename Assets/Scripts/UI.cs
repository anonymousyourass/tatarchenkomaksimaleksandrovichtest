using System.Collections.Generic;
using System.Linq;
using Prefabs.View;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
//todo: добавить class in UnityEngine/Implemented in:UnityEngine.JSONSerializeModule для сохранения/загрузки не в редакторе (в сборке для платформ) и в редакторе новых объектов
namespace Game
{
    public class UI : MonoBehaviour
    {
        public Button MainEnter;
        public Button ExploreExit;
        public Button PlusVehicle;
        public Button MinusVehicle;
        
        public GameObject Main;
        public GameObject Explore;
        
        public Transform  VehicleContent;
        
        public VehicleView vehicleUI;
        
        private List<Vehicle> _vehicles = new List<Vehicle>();
        private List<VehicleView> _vehiclesViews = new List<VehicleView>();
        
        async void Start()
        {
            var VehicleService = new VehicleService();
            await VehicleService.Initialize();
            VehicleService.Vehicles().ToList().ForEach(Add);
            
            Main.gameObject.SetActive(true);
            Explore.gameObject.SetActive(false);
                
            MainEnter.onClick.AddListener(OnClickMain);
            ExploreExit.onClick.AddListener(OnClickExploreExit);
            PlusVehicle.onClick.AddListener(OnClickAdd);
            MinusVehicle.onClick.AddListener(OnClickRemove);
        }

        private void OnClickMain()
        {
            Explore.gameObject.SetActive(true);
            Main.gameObject.SetActive(false);
        }
        
        private void OnClickExploreExit()
        {
            Explore.gameObject.SetActive(false);
            Main.gameObject.SetActive(true);
        }
        
        private void OnClickAdd()
        {
            var vehicle = ScriptableObject.CreateInstance<Vehicle>();
            Add(vehicle);
        }
        
        private void OnClickRemove()
        {
            RemoveLast();
        }

        private void Add(Vehicle vehicle)
        {
            var view = GameObject.Instantiate(vehicleUI, VehicleContent);
            view.ModelNameInput.text = vehicle.GetTitle();
            view.ModelNameInput.onValueChanged.AddListener(OnModelNameChanged);
            view.Image.sprite = vehicle.GetIcon();
            view.Field1.text = vehicle.GetField1();
            view.Field2.text = vehicle.GetField2();
            view.Field3.text = vehicle.GetField3();
            view.Field4.text = vehicle.GetField4();
            view.gameObject.SetActive(true);


            void OnModelNameChanged(string text)
            {
                vehicle.SetTitle(text);
                Debug.Log($"Название модели изменено на {text}");
            }
            
            _vehiclesViews.Add(view);
            _vehicles.Add(vehicle);

            RefreshContent();
        }

        void RefreshContent()
        {
            var rectTransform = (VehicleContent as RectTransform);
            rectTransform.offsetMax  = new Vector2(_vehiclesViews.Count * vehicleUI.gameObject.GetComponent<LayoutElement>().minWidth, rectTransform.offsetMax.y);
        }
        
        private void RemoveLast()
        {
            var vehicleView = _vehiclesViews.LastOrDefault();
            if (vehicleView == null)
                return;
            
            var vehicle = _vehicles.LastOrDefault();
            if (vehicleView == null || vehicle == null)
            {
                return;
            }
            
            Assert.IsTrue(vehicle != null && vehicleView != null);
            
            GameObject.Destroy(vehicleView.gameObject);

            vehicleView.ModelNameInput.onValueChanged.RemoveAllListeners();
            
            _vehiclesViews.Remove(vehicleView);
            _vehicles.Remove(vehicle);

            RefreshContent();
        }
        
        
        private void OnDestroy()
        {
            MainEnter.onClick.RemoveAllListeners();
            ExploreExit.onClick.RemoveAllListeners();
            PlusVehicle.onClick.RemoveAllListeners();
            MinusVehicle.onClick.RemoveAllListeners();
        }
    }
}

