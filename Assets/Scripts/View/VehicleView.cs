using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Prefabs.View
{
    public class VehicleView : MonoBehaviour
    {
        //public TextMeshProUGUI ModelName;
        public Image Image;
        public TextMeshProUGUI Field1;
        public TextMeshProUGUI Field2;
        public TextMeshProUGUI Field3;
        public TextMeshProUGUI Field4;
        
        public TMP_InputField ModelNameInput;
    }
}

