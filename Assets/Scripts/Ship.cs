using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "Ship", menuName = "ScriptableObjects/SpawnShip", order = 1)]
    public class Ship : Vehicle
    {
        [SerializeField] private int DisplacementMetersCube;
        
        public override string GetField4()
        {
            return $"Водоизмещение - {DisplacementMetersCube}";
        }
    }    
}


