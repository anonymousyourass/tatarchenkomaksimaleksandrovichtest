using UnityEngine;

namespace Game
{
    public class Vehicle : ScriptableObject
    {
        [SerializeField] private int Mass;
        [SerializeField] private int MaxSpeed;
        [SerializeField] private int Capacity;
        [SerializeField] private string ModelName;
        [SerializeField] private Sprite Sprite;

        public string GetTitle()
        {
            return $"{ModelName}";
        }
        
        public Sprite GetIcon()
        {
            return Sprite;
        }
        
        public string GetField1()
        {
            return $"Масса - {Mass}";
        }
        
        public string GetField2()
        {
            return $"Вместимость - {Capacity}";
        }
        
        public string GetField3()
        {
            return $"Макс. скорость  - {MaxSpeed}";
        }
        
        public virtual string GetField4()
        {
            return string.Empty;
        }
        
        public void SetTitle(string value)
        {
            ModelName = value;
        }
    }
}



