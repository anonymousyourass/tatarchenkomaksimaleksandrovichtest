using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "Airplane", menuName = "ScriptableObjects/SpawnAirplane", order = 1)]
    public class Airplane : Vehicle
    {
        [SerializeField] private int LiftingForceNewtons;
        
        public override string GetField4()
        {
            return $"Подъемная сила - {LiftingForceNewtons}";
        }
    }
}