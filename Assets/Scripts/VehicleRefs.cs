using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "Ref", menuName = "ScriptableObjects/SpawnRef", order = 1)]
    public class VehicleRefs : ScriptableObject
    {
        public List<Vehicle> AllVehicles; 
    }    
}


