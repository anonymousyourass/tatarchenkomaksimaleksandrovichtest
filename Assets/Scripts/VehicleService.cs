using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;

namespace Game
{
    public class VehicleService //Сервис при инициализации которого происходит загрузка из удаленного ресурса Addressables
    {
        private List<Vehicle> _vehicles;

        public async Task Initialize()//async Task Initialize Start Awake Enter Load OnServerCallback Action event callback итд точка входа для подгрузки данных базы  
        {
            var allRefsDailys = await Addressables.LoadAssetAsync<VehicleRefs>(Constants.DB_REFERENCE_ADDRESABLE).Task;
            _vehicles = allRefsDailys.AllVehicles;
        }
        
        public IEnumerable<Vehicle> Vehicles()
        {
            return _vehicles;
        }
    }
}




